# [Caddy](https://caddyserver.com) Debian/Ubuntu Packages

Pulls in latest tagged deb file from [caddy releases](https://github.com/caddyserver/caddy/releases) to provide a repo which is useful for e.g. `unattended-upgrades`, deployment via configuration management like Ansible/Saltstack/... and so on.

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-caddy.asc https://packaging.gitlab.io/caddy/gpg.key
```

## Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/caddy caddy main" | sudo tee /etc/apt/sources.list.d/morph027-caddy.list
```

## Install packages

```bash
sudo apt-get update
sudo apt-get install caddy morph027-keyring
```


## Extras

### unattended-upgrades

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"morph027:caddy";};' > /etc/apt/apt.conf.d/50caddy
```
